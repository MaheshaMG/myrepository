package com.ibm.crud.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibm.crud.entity.Employee;
import com.ibm.crud.service.EmployeeService;

@RestController
@RequestMapping(path = "/employee")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@GetMapping(path = "/")
	public String testController() {
		return "Employee Controller is running..";
	}
	
	@PostMapping(path = "/create")
	public Employee createEmployee(@RequestBody Employee employee) throws Exception{
		
		if (null == employee) {
			return null;
		}
		employee.setDateOfBirth(LocalDate.now());
		Employee employee2 = employeeService.createEmployee(employee);
		System.out.println(employee2);
		return employee2;
	}
	
	@GetMapping(path = "/list-employee")
	public List<Employee> getEmployeeList() throws Exception{
		return employeeService.getEmployeeList();
	}
	

}
