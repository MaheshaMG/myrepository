package com.ibm.crud.service;

import java.util.List;

import com.ibm.crud.entity.Employee;

public interface EmployeeService {
	
	public Employee createEmployee(Employee employee) throws Exception;
	
	public List<Employee> getEmployeeList() throws Exception;
}
