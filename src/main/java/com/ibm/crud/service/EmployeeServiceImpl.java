package com.ibm.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibm.crud.dao.EmployeeRepository;
import com.ibm.crud.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Employee createEmployee(Employee employee) throws Exception{
		 return employeeRepository.save(employee);
	}
	
	@Override
	public List<Employee> getEmployeeList() throws Exception{
		return employeeRepository.findAll();
	}
}
